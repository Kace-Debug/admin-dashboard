import React, { useContext } from 'react';
import AppMenuitem from './AppMenuitem';
import { LayoutContext } from './context/layoutcontext';
import { MenuProvider } from './context/menucontext';
import Link from 'next/link';
import { AppMenuItem } from '../types/types';

const AppMenu = () => {
    const { layoutConfig } = useContext(LayoutContext);

    const model: AppMenuItem[] = [
        {
            label: 'Home',
            items: [{ label: 'Dashboard', icon: 'pi pi-fw pi-home', to: '/dashboard' }]
        },

        {
            label: 'Add New DT-Services',
            items: [
                {
                    label: 'Add New DT-Services',
                    icon: 'pi pi-cog',
                    items: [
                        {
                            label: 'Add New Service',
                            icon: 'pi pi-android',
                            to:'/dashboard/newService'
                        },

                    ]
                },

            ]
        },


        {
            label: 'Services',
            items: [
                {
                    label: 'DT-Services',
                    icon: 'pi pi-th-large',
                    items: [
                        {
                            label: 'Real Estate',
                            icon: 'pi pi-android',
                        },
                        {
                            label: 'Chat-Bot',
                            icon: 'pi pi-android',
                        },
                        {
                            label: 'Project Management',
                            icon: 'pi pi-android',
                        },
                        {
                            label: 'marketing Service',
                            icon: 'pi pi-android',
                        }
                    ]
                },

            ]
        },


        {
            label: 'User-Profile',
            items: [
                { label: 'Configuration', icon: 'pi pi-users', to: '/configuration' },
                { label: 'Profile', icon: 'pi pi-users', to: '/profile' }
            ],

        },


        {
            label: 'Support',
            items: [

                { label: 'Help', icon: 'pi pi-question-circle', to: '/help' },
                { label: 'Dark Mode', icon: 'pi pi-moon',  },
            ],

        },

        {
            label: 'Pages',
            icon: 'pi pi-fw pi-briefcase',
            to: '/pages',
            items: [
                {
                    label: 'Auth',
                    icon: 'pi pi-fw pi-user',
                    items: [
                        {
                            label: 'Login',
                            icon: 'pi pi-fw pi-sign-in',
                            to: '/auth/login'
                        },
                        {
                            label: 'Error',
                            icon: 'pi pi-fw pi-times-circle',
                            to: '/auth/error'
                        },
                        {
                            label: 'Access Denied',
                            icon: 'pi pi-fw pi-lock',
                            to: '/auth/access'
                        }
                    ]
                },
        ]

        },

    ];

    return (
        <MenuProvider>
            <ul className="layout-menu">
                {model.map((item, i) => {
                    return !item?.seperator ? <AppMenuitem item={item} root={true} index={i} key={item.label} /> : <li className="menu-separator"></li>;
                })}

            </ul>
        </MenuProvider>
    );
};

export default AppMenu;
