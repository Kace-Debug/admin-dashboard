import React, {useState} from "react";
import {Dialog} from "primereact/dialog";
import {Button} from "primereact/button";
import {InputText} from "primereact/inputtext";
import Logo from "../../public/layout/images/logo.png"
import Image from 'next/image';

const AddNewService = ( ) => {
    const [displayBasic, setDisplayBasic] = useState(false);
    const basicDialogFooter = <Button type="button" label="OK" onClick={() => setDisplayBasic(false)} icon="pi pi-check" severity="secondary" />;

    return(
            <div className="grid">
                <div className="col-12 lg:col-12">
                    <div className="card">
                        <h6 className="font-bold">Add New Service</h6>
                        <Dialog visible={displayBasic} style={{ width: '30vw',}} modal  onHide={() => setDisplayBasic(false)}>
                                <div className="justify-content-center flex m-4">
                                    <Image src={Logo} alt="logo" width={40} height={30}/>
                                </div>
                            <h6 className="font-bold text-center">Add New Service</h6>
                            <div className="grid p-fluid">
                                <div className="col-12 md:col-12 p-5">
                                    <div className="field mb-5">
                                        <label htmlFor="name1">Email</label>
                                        <InputText id="name1" type="text" placeholder='Email' className='p-3'/>
                                    </div>
                                    <div className="field mb-5">
                                        <label htmlFor="email1">Service</label>
                                        <InputText id="email1" type="text" placeholder='Service' className='p-3'/>
                                    </div>
                                    <div className="field mb-5">
                                        <label htmlFor="email1">Base Url</label>
                                        <InputText id="email1" type="text" placeholder='Base Url' className='p-3' />
                                    </div>
                                    <div className="field mb-5">
                                        <label htmlFor="email1">Email Service</label>
                                        <InputText id="email1" type="text" placeholder='Email Service' className='p-3' />
                                    </div>
                                   <div className='field mt-8'>
                                       <button className='cursor-pointer justify-content-center font-bold text-white text-lg w-full flex px-4 py-3 border-none border-round bg-purple-600'
                                               type="button"
                                               onClick={() => setDisplayBasic(false)}>
                                           submit
                                       </button>;
                                   </div>
                                </div>

                            </div>

                        </Dialog>


                        <div className="grid">

                            <div className="col-12">
                                <Button type="button"
                                        label="add service" icon="pi pi-cloud-upload" className="bg-purple-600 border-none" onClick={() => setDisplayBasic(true)} />
                            </div>
                        </div>
                    </div>

                </div>

                <div className="col-12 md:col-3 text-center cursor-pointer">
                    <div className="card p-fluid shadow-3">

                      <div className="flex justify-content-center">
                          <div className="border-circle w-7rem h-7rem  bg-primary-100 font-bold flex align-items-center justify-content-center">
                              <i className="pi pi-spin pi-home" style={{ fontSize: '2rem' }}></i>
                          </div>
                      </div>
                                <div className="p-2">
                                    <h6 className="mt-4">Housing Estate</h6>
                                    <span>housing.com</span>
                                </div>
                        <button className="p-3 pl-6 pr-6 cursor-pointer border-round-xl border-purple-500 bg-white border-round-xs">Configure</button>
                    </div>
                </div>
                <div className="col-12 md:col-3 text-center cursor-pointer">
                    <div className="card p-fluid shadow-3">

                        <div className="flex justify-content-center">
                            <div className="border-circle w-7rem h-7rem  bg-purple-100 font-bold flex align-items-center justify-content-center">
                                <i className="pi pi-spin pi-comments" style={{ fontSize: '2rem' }}></i>
                            </div>
                        </div>
                        <div className="p-2">
                            <h6 className="mt-4">Chat Bot</h6>
                            <span>info.chatbot.ng</span>
                        </div>
                        <button className="p-3 pl-6 pr-6 cursor-pointer border-round-xl border-purple-500 bg-white border-round-xs">Configure</button>
                    </div>
                </div>

                <div className="col-12 md:col-3 text-center cursor-pointer">
                    <div className="card p-fluid shadow-3">

                        <div className="flex justify-content-center">
                            <div className="border-circle w-7rem h-7rem  bg-orange-100 font-bold flex align-items-center justify-content-center">
                                <i className="pi pi-spin pi-home" style={{ fontSize: '2rem' }}></i>
                            </div>
                        </div>
                        <div className="p-2">
                            <h6 className="mt-4">Marketing / Management</h6>
                            <span>dtworld.com</span>
                        </div>
                        <button className="p-3 pl-6 pr-6 cursor-pointer border-round-xl border-purple-500 bg-white border-round-xs">Configure</button>
                    </div>
                </div>
                <div className="col-12 md:col-3 text-center cursor-pointer">
                    <div className="card p-fluid shadow-3">

                        <div className="flex justify-content-center">
                            <div className="border-circle w-7rem h-7rem  bg-indigo-100 font-bold flex align-items-center justify-content-center">
                                <i className="pi pi-spin pi-home" style={{ fontSize: '2rem' }}></i>
                            </div>
                        </div>
                        <div className="p-2">
                            <h6 className="mt-4">Housing Estate</h6>
                            <span>housing.com</span>
                        </div>
                        <button className="p-3 pl-6 pr-6 cursor-pointer border-round-xl border-purple-500 bg-white border-round-xs">Configure</button>
                    </div>
                </div>


            </div>
    )
}

export default AddNewService